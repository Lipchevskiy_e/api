<?php

namespace App\Models\API\V1;

use App\Filters\CarFilter;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class CarsModel extends Model
{
    use Filterable;
    protected $table = 'cars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'state_number', 'color', 'vin_code', 'brand', 'model', 'year'
    ];

    public function modelFilter()
    {
        return $this->provideFilter(CarFilter::class);
    }

}
