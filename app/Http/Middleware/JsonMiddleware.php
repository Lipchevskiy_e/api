<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;
use Illuminate\Http\Request;

class JsonMiddleware
{
    /**
     * @param Request $request
     * @param \Closure $next
     */
    public function handle($request, \Closure $next)
    {
        $request->headers->set('Accept','application/json');
        return $next($request);
    }
}
