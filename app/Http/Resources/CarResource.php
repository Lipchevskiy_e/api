<?php

namespace App\Http\Resources;

use App\Models\API\V1\CarsModel;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @mixin CarsModel
 */
class CarResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'state_number'  => $this->state_number,
            'color'         => $this->color,
            'vin_code'      => $this->vin_code,
            'brand'         => $this->brand,
            'model'         => $this->model,
            'year'          => $this->year,
            'created_at' => $this->created_at,
        ];
    }
}
