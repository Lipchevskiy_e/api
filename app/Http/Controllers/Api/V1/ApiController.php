<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\API\V1\ApiModel;
use App\Services\CarService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\API\V1\CarsModel;
use App\Http\Resources\CarResource;
use App\Http\Requests\CarsRequest;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) :AnonymousResourceCollection
    {
        $query = CarsModel::query()
            ->filter($request->only(['query', 'brand', 'model', 'year']));

        if($sort = $request->input('sort')) {
            $sortData =  explode('-',$sort);
            $field = array_shift($sortData);
            $direction = array_shift($sortData);
            $query->orderBy($field,$direction);
        }
        return CarResource::collection(
            $query->paginate()
        );
    }


    public function store(CarsRequest $request, CarService $carService): CarResource
    {
        $car = CarsModel::query()->create(array_merge($request->validated(),$carService->getCarByVin($request->input('vin_code'))));
        return CarResource::make($car);
    }

    public function update(int $id,CarsRequest $request, CarService $carService)
    {
        $car = CarsModel::query()->findOrFail($id);
        $car->fill(array_merge($request->validated(),$carService->getCarByVin($request->input('vin_code'))));
        $car->save();
        return CarResource::make($car);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        if($car = CarsModel::query()->findOrFail($id))
            $car->delete();
    }

}
