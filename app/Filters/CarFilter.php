<?php


namespace App\Filters;


use EloquentFilter\ModelFilter;

class CarFilter extends ModelFilter
{

    public function query(string $query)
    {
        $this
            ->orWhere('name','LIKE','%'.$query.'%')
            ->orWhere('state_number','LIKE','%'.$query.'%')
            ->orWhere('vin_code','LIKE','%'.$query.'%')
        ;

    }
    public function brand(string $brand)
    {
        $this->where('brand','LIKE','%'.$brand.'%');

    }
    public function model(string $model)
    {
        $this->where('model','LIKE','%'.$model.'%');

    }
    public function year(int $year)
    {
        $this->where('year',$year);
    }





}
