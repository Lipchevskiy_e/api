<?php

namespace App\Services;

use GuzzleHttp\Client;

class CarService
{

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();
    }
    public function getCarByVin(string $vin)
    {
        $result = [];
        $url = 'https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/'.$vin.'?format=json';
        $response = $this->client->request('GET',$url);
        if ($response->getStatusCode()  == 200) {
            $result = array_shift(json_decode($response->getBody()->getContents())->Results);
            $result= [
                'model' => $result->Model,
                'year'  => $result->ModelYear,
                'brand' => $result->Make,
            ];
        }
        return $result;
    }


}
