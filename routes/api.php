<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// version 1
Route::get('v1/cars',[ApiController::class,'index']);
Route::post('v1/cars',[ApiController::class,'store']);
Route::delete('v1/cars/{id}',[ApiController::class,'destroy']);
Route::put('v1/cars/{id}',[ApiController::class,'update']);

